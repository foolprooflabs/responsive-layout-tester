<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Responsive Website Tester - Foolproof Labs</title>
    <link rel="shortcut icon" href="http://labs.responsivetester.foolprooflabs.com/img/logo.ico">
     
     <meta name="description" content="An easy to use website tester for checking how your site displays on different resolutions by Foolproof Labs" />
     <meta property="og:image" content="http://labs.responsivetester.foolprooflabs.com/img/f_logo.png"/>
     <meta property="og:title" content="Responsive Website Tester - Foolproof Labs"/>
     <meta property="og:url" content="http://labs.responsivetester.foolprooflabs.com/"/>
     <meta property="og:site_name" content="Responsive Website Tester - Foolproof Labs"/>
     <meta property="og:type" content="website"/>
   
    <link rel="stylesheet" href="css/combination.css" />
    <link href='http://fonts.googleapis.com/css?family=Montserrat+Alternates:700' rel='stylesheet' type='text/css'>
    <script src="js/myJs.js"></script>
    <script src="js/jquery.js"></script>

  <script type="text/javascript" src="js/smoothscroll.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-44061922-4', 'foolprooflabs.com');
  ga('send', 'pageview');

</script>

</head>


<div class="top-bar fixed" data-topbar>
   
        <div class="left">
           <img src="img/logo2.png">
         </div>

              <div class="search"> 
                         <a href="#" class="button postfix btnSearch" onclick="changeUrl()" >Test</a> 
                           <input type="text"  placeholder="Place website URL here" class="txtSearch" id="url">
              </div>


            <section class="top-bar-section">
    <!-- Right Nav Section -->
            <ul class="right my-left-nav show-for-large-up">
       
                      <li><a href="#s240x320" class="smoothScroll" >240x320</a></li> 
                      <li class="divider"></li>
                      <li><a href="#s320x480" class="smoothScroll">320x480</a> </li>
                      <li class="divider"></li>
                      <li><a href="#s480x640" class="smoothScroll">480x640</a></li>
                      <li class="divider"></li>
                      <li><a href="#s768x1024" class="smoothScroll">768x1024</a></li>
                      <li class="divider"></li>
                      <li><a href="#s1024x768" class="smoothScroll">1024x768</a></li> 
                 </ul> 
          </section>       


               <section class="top-bar-section show-for-medium-down right">
    <!-- Right Nav Section -->
                            <select size="1" name="links" onchange="window.location.href=this.value;">
                                      <option value="#s240x320">240x320</option>
                                      <option value="#s320x480">320x480</option>
                                      <option value="#s480x640">480x640</option>
                                      <option value="#s768x1024">768x1024</option>
                                      <option value="#s1024x768">1024x768</option>
                            </select>
              </section>                 
    

</div>



 <div id="s240x320" class="row div1Box">

    <div class="div1">
    <iframe src="index2.html" id="frame1" class="frame1"></iframe>
       <p class="text-center">240x320 portrait</p>
    </div>   
</div>


  


<div id="s320x480" class="row div2Box">
  <div class="div2">
    <iframe src="index2.html" id="frame2" class="frame2 "></iframe>
      <p class="text-center">320x480 portrait</p>
    </div>
</div>



<div id="s480x640" class="row div3Box">
  <div class="div3">
    <iframe src="index2.html" id="frame3" class="frame3"></iframe>
      <p class="text-center">480x640 portrait</p>
   </div>   
</div>


<div id="s768x1024" class="row div4Box">
  <div class="div4">
    <iframe src="index2.html" id="frame4" class="frame4"></iframe>
      <p class="text-center">768x1024 portrait</p>
     </div> 
  
</div>


<div id="s1024x768" class="div5Box">
  <div class="div5">
    <iframe src="index2.html" id="frame5" class="frame5"></iframe>
      <p class="text-center">1024x768 portrait</p>
     </div> 
</div>

</body>
</html>